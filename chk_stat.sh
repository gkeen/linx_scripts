#!/bin/sh
######################################################
# Gary Keen
# 1/27/16
#
# Loop that runs through top,vmstat and free to capture
# a look at the current system statistics.
#
######################################################
#
echo
echo "This script will loop through several system tools."
echo "	        top --- vmstat --- free "
echo
echo "Press enter to continue..."
read X
#
while true
do

top -n 5 
#
sleep 4
echo
date
vmstat -w 2 10
echo
sleep 4
free -mls 2 -c 5
echo
sleep 4

done
