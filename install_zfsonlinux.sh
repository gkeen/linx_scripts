#!/bin/sh
##############################################################
# Gary Keen
# 1/8/16
#
# Use the existing yum repo to install zfs,dkm and spl.
#
#
##############################################################
echo 
echo "  YOU MUST BE ROOT TO RUN THIS SCRIPT."
echo " This script is CENTOS 6 or 7 specific. "
echo
sleep 1
#
#
#
centos7 () {
echo
echo "This will install zfs on your Centos 7 system."
echo " If you are NOT  on a centos 7 system. Turn back now."
echo 
echo "Press enter to continue."
read X
yum localinstall --nogpgcheck https://download.fedoraproject.org/pub/epel/7/x86_64/e/epel-release-7-5.noarch.rpm -y
yum localinstall --nogpgcheck http://archive.zfsonlinux.org/epel/zfs-release.el7.noarch.rpm -y
yum install kernel-devel zfs -y
}
#
centos6 () {
echo
echo "This will install zfs on your Centos 6 system."
echo " If you are NOT  on a centos 6 system. Turn back now."
echo 
echo "Press enter to continue."
read X
yum localinstall --nogpgcheck https://download.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm -y
yum localinstall --nogpgcheck http://archive.zfsonlinux.org/epel/zfs-release.el6.noarch.rpm -y
yum install kernel-devel zfs -y
}
#
#
echo
echo "What Centos version are you running ?  centos 6  or  centos 7 ?"
echo "Enter  6   or   7 "
echo
read OS
echo
echo "Install zfs on centos $OS " 
echo "If this is correct. Press enter. Otherwise cntrol C"
echo
#
#
case $OS in
	6) centos6
	;;
	7) centos7
	;;
	*) echo "Usage: 6    or    7 "
	;;
esac



#------------------------------------ EOF ------------------------------#
