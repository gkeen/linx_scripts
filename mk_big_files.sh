#!/bin/sh
############################################
# Gary Keen
#
#
# Loop a build a bunch of 2Gb bigfiles
#
############################################
#
ddfiles () {
for A in 1
do

echo "Start build bigfiles......"
#

FILE=/var/tmp/bigfile$A
# dd if=/dev/zero of=$FILE bs=2048k count=1000 &
dd if=/dev/zero of=$FILE bs=1024M count=12
ls -lh /var/tmp |grep bigfile
echo
df -h /var/tmp
echo
sleep 5
done
removefiles
}
#
removefiles () {
ps aux |grep dd | grep zero
EXIT=$?
if [ $EXIT = 0 ]
then
echo
echo "Waiting for file building to complete."
echo
sleep 10
removefiles
else
echo "Removing created files...."
rm -rf /var/tmp/bigfile*
echo "ls -l /var/tmp"
ls -l /var/tmp
sleep 3
fi
ddfiles
}
#

ddfiles


#----------------------- EOF ----------------#
