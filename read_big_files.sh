#!/bin/sh
############################################
# Gary Keen
#
#
# Build 1Gb files, and copy them in a loop 
#
############################################
#
ddfiles () {
echo
echo "Start build bigfiles......"
echo
sleep 2
for A in 1 2 3 4 5
do
FILE=/var/tmp/bigfile$A
dd if=/dev/zero of=$FILE bs=1024k count=1000 &
ls -lh /var/tmp |grep bigfile
echo
df -h /var/tmp
echo
sleep 5
done
echo
echo "==============================================================="
df -h /var/tmp
echo "==============================================================="
cpfiles
}
#
cpfiles () {
echo
echo "Start copying files......"
echo
sleep 2
for B in 1 2 3 4 5
do 
COPY=/var/tmp/bigfile$B
cp $COPY $COPY.copy &
ls -lht /var/tmp
done
echo
echo "==============================================================="
df -h /var/tmp
echo "==============================================================="
sleep 3
removefiles
}

#
removefiles () {
ps aux |grep cp | grep copy
EXIT=$?
if [ $EXIT = 0 ]
then
echo
echo "Waiting for file copy to complete."
echo "==========================================================="
df -h /var/tmp/
echo "==========================================================="
echo
sleep 10
removefiles
else
echo "Removing created files...."
rm -rf /var/tmp/bigfile*
echo "ls -l /var/tmp"
ls -l /var/tmp
sleep 3
fi
ddfiles
}
#

ddfiles


#----------------------- EOF ----------------#
